<?php
declare(strict_types=1);

namespace Wallet;

use App\Controller\ApiController;
use Cake\Routing\RouteBuilder;
use RestApi\Lib\RestPlugin;

class WalletPlugin extends RestPlugin
{
    protected function routeConnectors(RouteBuilder $builder): void
    {
        $builder->connect('/users/{userID}/issuance/*', \Wallet\Controller\WalletIssuanceController::route());
        $builder->connect('/credentialSchemas/*', \Wallet\Controller\WalletCredentialSchemaController::route());
        $builder->connect('/verifier/present/*', \Wallet\Controller\VerifierPresentProxyController::route());
        $builder->connect('/verifier/verify/*', \Wallet\Controller\VerifierVerifyProxyController::route());
        $builder->connect('/verifier/store/*', \Wallet\Controller\VerifierStoreController::route());
        $builder->connect('/openapi/*', \Wallet\Controller\SwaggerJsonController::route());
    }
}
