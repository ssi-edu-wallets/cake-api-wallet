<?php

declare(strict_types = 1);

namespace Wallet\Controller;

use Wallet\WalletPlugin;

class SwaggerJsonController extends \RestApi\Controller\SwaggerJsonController
{
    protected function getContent(\RestApi\Lib\Swagger\SwaggerReader $reader, array $paths): array
    {
        $host = $_SERVER['HTTP_HOST'] ?? 'example.com';
        $plugin = WalletPlugin::getRoutePath();
        $url = 'https://' . $host . $plugin . '/';
        return [
            'openapi' => '3.0.0',
            'info' => [
                'version' => '0.1.1',
                'title' => 'EduWallet plugin',
                'description' => 'Wallet demo plugin',
                'termsOfService' => 'https://www.eduplex.eu/impressum/',
                'contact' => [
                    'name' => 'Development in Gitlab',
                    'url' => 'https://gitlab.com/ssi-edu-wallets/cake-api-wallet',
                ],
                'license' => [
                    'name' => 'MIT License',
                    'url' => 'https://opensource.org/licenses/MIT',
                ],
            ],
            'servers' => [
                ['url' => $url]
            ],
            'tags' => [],
            'paths' => $paths,
            'components' => [
                'securitySchemes' => [
                    'bearerAuth' => [
                        'type' => 'http',
                        'scheme' => 'bearer',
                    ]
                ],
            ],
        ];
    }
}
