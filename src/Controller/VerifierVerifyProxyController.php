<?php
declare(strict_types=1);

namespace Wallet\Controller;

use App\Controller\ApiController;
use Wallet\Lib\VerifierApiClient;

class VerifierVerifyProxyController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    public function addNew($data)
    {
        $api = new VerifierApiClient($this->request);
        $redirect = $api->postVerify($data);
        $this->redirect($redirect);
    }
}
