<?php
declare(strict_types=1);

namespace Wallet\Controller;

use App\Controller\ApiController;
use Wallet\Lib\VerifierApiClient;

class VerifierPresentProxyController extends ApiController
{
    public function isPublicController(): bool
    {
        return true;
    }

    public function getList()
    {
        $api = new VerifierApiClient($this->request);
        $this->redirect($api->getPresent());
    }
}
