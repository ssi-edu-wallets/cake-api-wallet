<?php
declare(strict_types=1);

namespace Wallet\Lib\VCs;

use App\Lib\FullBaseUrl;
use Assessment\Model\Entity\InitiativesUser;
use Assessment\Model\Entity\Submission;
use Cake\Http\Client;

class VcEuropass implements VcInterface
{
    /** @var Submission */
    private $_submission;
    /** @var int */
    private $_assessmentIndex = 1;
    private $_cachedTags = [];

    public function setSubmission(Submission $submission): VcEuropass
    {
        $this->_submission = $submission;
        return $this;
    }

    public static function getType(): string
    {
        return 'Europass';
    }

    private function _template(array $subject): array
    {
        return [
            'credentials' => [
                [
                    'credentialData' => [
                        'credentialSubject' => $subject
                    ],
                    'type' => $this->getType()
                ]
            ]
        ];
    }

    private function _assessment(InitiativesUser $initiativeUser): array
    {
        $http = new Client();
        $outcomes = [];
        foreach ($initiativeUser->initiative->tags_list as $tagName) {
            $urlTag = urlencode($tagName);
            if (!isset($this->_cachedTags[$urlTag])) {
                $url = FullBaseUrl::host() . '/api/v2/tags/' . urlencode($urlTag);
                try {
                    $res = $http->get($url);
                    $this->_cachedTags[$urlTag] = $res->getJson()['data'] ?? '';
                } catch (\Exception $e) {
                    $this->_cachedTags[$urlTag] = '';
                }
            }
            if ($this->_cachedTags[$urlTag]) {
                $outcomes[] = $this->_learningOutcome($this->_cachedTags[$urlTag]);
            }
        }
        $res = [
            'id' => 'urn:epass:assessment:'.$this->_assessmentIndex++,
            'title' => $initiativeUser->initiative->title,
            'definition' => $initiativeUser->initiative->description,
            'grade' => round($initiativeUser->percentage / 10, 1),
        ];
        if ($outcomes) {
            $res['specifiedBy'] = [ // Assessment Specification
                'proves' => [
                    0 => [ // Learning Specification
                        //'title' => 'Verarbeitung digitaler Daten ACTION INITIATIVES',
                        //'definition' => 'Short and abstract description about the learning specification',
                        //'learningOutcomeDescription' => '',
                        'learningOutcome' => $outcomes
                    ]
                ]
            ];
        }
        return $res;
    }

    private function _assessmentWithParts(Submission $submission): array
    {
        $res = [
            'id' => 'urn:epass:assessment:'.$this->_assessmentIndex++,
            'title' => $submission->questionnaire->title,
            'definition' => 'Basic assessment from LXP',
        ];
        $sumOfScores = 0;
        $sumOfMaxScores = 0;
        $parts = [];
        foreach ($submission->initiatives_users as $initiative) {
            $parts[] = $this->_assessment($initiative);
            $sumOfScores += $initiative->score;
            $sumOfMaxScores += $initiative->max_score;
        }
        if ($sumOfMaxScores) {
            $res['grade'] = round($sumOfScores / $sumOfMaxScores * 10);
        } else {
            $res['grade'] = 0;
        }
        $res['hasPart'] = $parts;
        return $res;
    }

    private function _learningOutcome(array $tags): array
    {
        return [ // Learning Outcome
            'id' => 'uuid:lerningSpecification',
            'name' => $tags['display_name'] ?? '',
            'definition' => $tags['header'] ?? '',
            'relatedESCOSkill' => [$tags['esco_uri'] ?? '']
        ];
    }

    public function toArray(): array
    {
        $subject = [
            'id' => '',
            'title' => 'LXP skill assessment complete',
            'achieved' => [
                [ // Learning Achievement
                    'id' => 'urn:epass:learningAchievement:1',
                    'title' => 'Data and software business diploma',
                    'wasDerivedFrom' => [
                        $this->_assessmentWithParts($this->_submission)
                    ]
                ]
            ]
        ];
        return $this->_template($subject);
    }
    /*
     * Cloned schema from https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/Europass.json
     */
    public function vcSchema(): array
    {
        return [
            '$schema' => 'http://json-schema.org/draft-07/schema#',
            'type' => 'object',
            'properties' => [
                '@context' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'string'
                    ]
                ],
                'credentialSchema' => [
                    'type' => 'object',
                    'properties' => [
                        'id' => [
                            'type' => 'string'
                        ],
                        'type' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'credentialStatus' => [
                    'type' => 'object',
                    'properties' => [
                        'id' => [
                            'type' => 'string'
                        ],
                        'type' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'credentialSubject' => [
                    'type' => 'object',
                    'properties' => [
                        'awardingOpportunity' => [
                            'type' => 'object',
                            'properties' => [
                                'awardingBody' => [
                                    'type' => 'object',
                                    'properties' => [
                                        'eidasLegalIdentifier' => [
                                            'type' => 'string'
                                        ],
                                        'homepage' => [
                                            'type' => 'string'
                                        ],
                                        'id' => [
                                            'type' => 'string'
                                        ],
                                        'preferredName' => [
                                            'type' => 'string'
                                        ],
                                        'registration' => [
                                            'type' => 'string'
                                        ]
                                    ],
                                    'additionalProperties' => false
                                ],
                                'endedAtTime' => [
                                    'type' => 'string'
                                ],
                                'id' => [
                                    'type' => 'string'
                                ],
                                'identifier' => [
                                    'type' => 'string'
                                ],
                                'location' => [
                                    'type' => 'string'
                                ],
                                'startedAtTime' => [
                                    'type' => 'string'
                                ]
                            ],
                            'additionalProperties' => false
                        ],
                        'dateOfBirth' => [
                            'type' => 'string'
                        ],
                        'familyName' => [
                            'type' => 'string'
                        ],
                        'givenNames' => [
                            'type' => 'string'
                        ],
                        'gradingScheme' => [
                            'type' => 'object',
                            'properties' => [
                                'description' => [
                                    'type' => 'string'
                                ],
                                'id' => [
                                    'type' => 'string'
                                ],
                                'title' => [
                                    'type' => 'string'
                                ]
                            ],
                            'additionalProperties' => false
                        ],
                        'id' => [
                            'type' => 'string'
                        ],
                        'identifier' => [
                            'type' => 'string'
                        ],
                        'learningAchievement' => [
                            'type' => 'object',
                            'properties' => [
                                'additionalNote' => [
                                    'type' => 'array',
                                    'items' => [
                                        'type' => 'string'
                                    ]
                                ],
                                'description' => [
                                    'type' => 'string'
                                ],
                                'id' => [
                                    'type' => 'string'
                                ],
                                'title' => [
                                    'type' => 'string'
                                ]
                            ],
                            'additionalProperties' => false
                        ],
                        'learningSpecification' => [
                            'type' => 'object',
                            'properties' => [
                                'ectsCreditPoints' => [
                                    'type' => 'integer'
                                ],
                                'eqfLevel' => [
                                    'type' => 'integer'
                                ],
                                'id' => [
                                    'type' => 'string'
                                ],
                                'iscedfCode' => [
                                    'type' => 'array',
                                    'items' => [
                                        'type' => 'string'
                                    ]
                                ],
                                'nqfLevel' => [
                                    'type' => 'array',
                                    'items' => [
                                        'type' => 'string'
                                    ]
                                ]
                            ],
                            'additionalProperties' => false
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'evidence' => [
                    'type' => 'object',
                    'properties' => [
                        'documentPresence' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'string'
                            ]
                        ],
                        'evidenceDocument' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'string'
                            ]
                        ],
                        'id' => [
                            'type' => 'string'
                        ],
                        'subjectPresence' => [
                            'type' => 'string'
                        ],
                        'type' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'string'
                            ]
                        ],
                        'verifier' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'expirationDate' => [
                    'type' => 'string'
                ],
                'id' => [
                    'type' => 'string'
                ],
                'issuanceDate' => [
                    'type' => 'string'
                ],
                'issued' => [
                    'type' => 'string'
                ],
                'issuer' => [
                    'type' => 'string'
                ],
                'proof' => [
                    'type' => 'object',
                    'properties' => [
                        'created' => [
                            'type' => 'string'
                        ],
                        'creator' => [
                            'type' => 'string'
                        ],
                        'domain' => [
                            'type' => 'string'
                        ],
                        'jws' => [
                            'type' => 'string'
                        ],
                        'nonce' => [
                            'type' => 'string'
                        ],
                        'proofPurpose' => [
                            'type' => 'string'
                        ],
                        'type' => [
                            'type' => 'string'
                        ],
                        'verificationMethod' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'type' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'string'
                    ]
                ],
                'validFrom' => [
                    'type' => 'string'
                ]
            ],
            'required' => ['@context', 'type'],
            'additionalProperties' => false
        ];
    }
}
