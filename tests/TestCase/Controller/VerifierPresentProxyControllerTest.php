<?php
declare(strict_types=1);

namespace Wallet\Test\TestCase\Controller;

use RestApi\TestSuite\ApiCommonErrorsTest;
use Wallet\WalletPlugin;

class VerifierPresentProxyControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
    ];

    protected function _getEndpoint(): string
    {
        return WalletPlugin::getRoutePath() . '/verifier/present/';
    }

    public function testGetList()
    {
        $this->get($this->_getEndpoint() . '?walletId=walt.id&vcType=VerifiableId');
        $this->assertRedirectContains('https://portal.walt.id/');
    }
}
