# Cake-API-Wallet

Wallet demo plugin for CakePHP

## Works with
CakePHP Plugin to run on top of [cake-rest-api](https://packagist.org/packages/freefri/cake-rest-api).

## Openapi documentation

Swagger UI in [/edu/api/v1/wallet/openapi/](https://proto.eduplex.eu/edu/api/v1/wallet/openapi/)

## License
The source code for the site is licensed under the **MIT license**, which you can find in the [LICENSE](../LICENSE/) file.

Initially released as [Demo Application](https://gitlab.com/ssi-edu-wallets/demo-application/) repository.
